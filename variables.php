<?php //Si el documento es íntegramente de PHP podemos omitir la etiqueta de cierre ?.>(sin el punto)

// Asignación
$num = 9;
$lang =[
    'es'=> 'español',
    'en'=> 'inglés',
];

// Aritmética
echo "Suma 2 + 2" . ((int)2 + (int)2); // El . es la forma de conectar las cadenas
echo "Resta 2 - 2" . ((int)2 - (int)2); // El cast (int) es simplemente para avisar del tipo de dato
echo "Multiplicación 2 * 2" . 2 * 2; // Como podemos ver no es obligatorio
echo "División 2 / 2" . 2 / 2;
echo "Módulo 2 % 2" . 2 % 2; // Muestra el resultado de la división
echo "Exponencial 2 ** 2" . 2 ** 2; // 2^2

// Comparación

// Igual ==, valor'9' == 9 --> true porque solo compara el valor
// Igual ===, valor tipo 9 === 9 --> true porque compara valor y tipo de dato (string, int, boolean, etc)
// Diferencias !=, valor 9 != 8
// Diferencias !==, valor y tipo 9 !== '9'

// Variables que varian
$app = 'name';
$name = 'Adri';
echo $app; // imprime name
echo $$app; //imprime Adri porque el valor de app es name y name es otra variable


/* 
    $lastName = 'Antañón';
    $nome = 'Adri' . $lastName;
    $anotherName = "Adri $lastName";
    Las comillas simples (’ ') se interpretan como texto plano. Como por ejemplo, el nombre agregado en la variable $name.
    Las comillas dobles (" ") intentan interpretar todo lo que encuentre dentro. Ejemplo: $anotherName.
 */