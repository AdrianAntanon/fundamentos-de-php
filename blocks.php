<?php

// If-elseif-else
if (1 !== 1) { //if(condición)
    #code...
} elseif (1 !== '1') {
    #code...
} else {
    #code...
}

// switch
switch ($login) {
    case true:
        #code...
        break;
    case false:
        #code...
        break;
    default:
        #code...
        break; //Creo que es opcional, ya que haya o no haya break el switch termina aquí ya que no hay más código
}

// forEach, su usa para realizar consultas, mejor que este lo explique el profesor, a mi no me queda del todo claro 
foreach($data as $row){
    #code...
}

// while
$a = 1;
while($a <= 10){
    #code...

}

// do-while
do {
    # code...
} while ($a <= 10);

// for
for ($i=0; $i < 10 ; $i++) { 
    # code...
}

// Operadores de incremento
/* 
++$a 	Pre-incremento 	Incrementa $a en uno, y luego retorna $a.
$a++ 	Post-incremento 	Retorna $a, y luego incrementa $a en uno.
--$a 	Pre-decremento 	Decrementa $a en uno, luego retorna $a.
$a-- 	Post-decremento 	Retorna $a, luego decrementa $a en uno.
 */

//  Curiosidad, operadores ternarios, son increíblemente sencillos
// Es un IF SIMPLIFICADO

// Condición ? Si es Verdadero : Si es Falso ;
echo $a === 'a' ? "CUMPLE LA CONDICIÓN" : "NO CUMPLE LA CONDICIÓN";